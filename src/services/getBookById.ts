import axios from 'axios'

export const getBookById = async (id: string | undefined): Promise<unknown> => {
  const API_KEY = process.env.API_KEY
  const response = await axios.get(
    `https://www.googleapis.com/books/v1/volumes/${id}?key=${API_KEY}&projection=full&download=epub`
  )
  const books = response?.data

  return books
}
