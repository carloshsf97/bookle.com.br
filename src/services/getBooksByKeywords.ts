import { BooksResponse, SuggestionResponse } from '@/interfaces/responses'
import axios from 'axios'

export const getBooksByKeywords = async (
  keyword?: string
): Promise<BooksResponse> => {
  const response = await axios.get(`/api/getBooksByKeyword/v1/${keyword}`)

  const books = response?.data?.books
  const totalItems = response?.data?.totalItems

  return { books, totalItems }
}

export const getSuggestionsByKeyword = async (
  keyword?: string
): Promise<SuggestionResponse> => {
  const response = await axios.get(`/api/getSuggestionsByKeyword/v1/${keyword}`)

  const suggestions = response?.data?.suggestions
  const totalItems = response?.data?.totalItems

  return { suggestions, totalItems }
}

export const getBooksByKeywordsGoogleService = async (
  keyword?: string,
  end = 20,
  start = 0
): Promise<BooksResponse> => {
  const API_KEY = process.env.API_KEY

  const response = await axios.get(
    `https://www.googleapis.com/books/v1/volumes?q=${keyword}&key=${API_KEY}&startIndex=${start}&maxResults=${end}`
  )

  const books = response?.data?.items
  const totalItems = response?.data?.totalItems

  return { books, totalItems }
}
