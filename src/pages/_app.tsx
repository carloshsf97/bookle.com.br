import { AppProps } from 'next/app'
import Head from 'next/head'
import ThemeWrapper from 'components/atoms/ThemeWrapper'

import { GlobalProvider } from 'contexts/globalContext'

import { fadeIn } from 'constants/animations'
import { motion } from 'framer-motion'

function App({ Component, pageProps, router }: AppProps) {
  return (
    <GlobalProvider>
      <ThemeWrapper>
        <Head>
          <title>Bookle</title>
          <link
            rel="shortcut icon"
            type="image/png"
            href="/images/bookle.png"
          />
          <link
            rel="apple-touch-icon"
            type="image/png"
            href="/images/bookle.png"
          />
          <link rel="manifest" href="/manifest.json" />
          <meta name="description" content="" />
          <meta name="theme-color" content="#fafafa" />
        </Head>
        <main>
          <motion.div
            key={router.pathname}
            animate="animate"
            initial="initial"
            variants={fadeIn}
          >
            <Component {...pageProps} />
          </motion.div>
        </main>
      </ThemeWrapper>
    </GlobalProvider>
  )
}

export default App
