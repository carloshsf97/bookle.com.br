import { getBooksByKeywordsGoogleService } from '@/services'
import { filterValidBooks, removeDuplicateBooks } from '@/utils/filters'
import type { NextApiRequest, NextApiResponse } from 'next'

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { keyword } = req.query
  const search = keyword.toString() || ''

  if (req.method === 'GET') {
    try {
      const response = await getBooksByKeywordsGoogleService(search)
      const filteredBooks = removeDuplicateBooks(
        filterValidBooks(response.books)
      )
      res
        .status(200)
        .json({ books: filteredBooks, totalItems: response.totalItems })
    } catch (err) {
      res.status(500).json({ error: true })
    }
  }
}
