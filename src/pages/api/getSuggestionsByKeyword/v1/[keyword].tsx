import { getBooksByKeywordsGoogleService } from '@/services'
import { formatBookResponseToSuggestions } from '@/utils'
import { filterValidBooks, removeDuplicateBooks } from '@/utils/filters'
import type { NextApiRequest, NextApiResponse } from 'next'

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { keyword } = req.query
  const search = keyword.toString() || ''

  if (req.method === 'GET') {
    try {
      const response = await getBooksByKeywordsGoogleService(search)
      const validBooks = filterValidBooks(response.books)
      const filteredBooks = formatBookResponseToSuggestions(
        removeDuplicateBooks(validBooks)
      )
      const length = filteredBooks ? filteredBooks?.length + 1 : 0
      res.status(200).json({ suggestions: filteredBooks, totalItems: length })
    } catch (err) {
      res.status(500).json({ error: true })
    }
  }
}
