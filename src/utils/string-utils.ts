export const truncateString = (str: string, num: number): string => {
  if (str.length <= num || num === 0) {
    return str
  }
  return `${str.slice(0, num)}...`
}

export const normalize = (str: string): string =>
  str.normalize('NFD').replace(/[\u0300-\u036f]/g, '')

export const capitalize = (str: string): string =>
  `${str.charAt(0).toUpperCase()}${str.slice(1).toLocaleLowerCase()}`

export const seoUrl = (str: string): string =>
  normalize(str)
    .toLowerCase()
    .replace(/[^a-z0-9]+/g, '-')
    .replace(/^-+|-+$/g, '-')
    .replace(/^-+|-+$/g, '')

export const pluralize = (string: string, count: number): string =>
  `${string}${count > 1 ? 's' : ''}`

export const removeHtmlWhiteSpace = (text: string): string => {
  return text.split(/\s+/).join(' ').replace(/>\s+</g, '><').trim()
}
