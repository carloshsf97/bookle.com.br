import { Book } from '@/interfaces'

export const filterValidBooks = (books?: Book[]): Book[] | undefined => {
  return books?.filter(
    each =>
      !!each.volumeInfo.imageLinks?.thumbnail &&
      !!each.volumeInfo?.description &&
      !!each.volumeInfo?.subtitle &&
      !!each.saleInfo
  )
}

export const removeDuplicateBooks = (books?: Book[]): Book[] | undefined => {
  const seen = new Set()
  return books?.filter(item =>
    seen.has(item.volumeInfo.title) ? false : seen.add(item.volumeInfo.title)
  )
}
