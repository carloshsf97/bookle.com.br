import { Suggestion } from '@/components'
import { Book } from '@/interfaces'

export const removeSuggestionFromList = (
  value?: string,
  suggestions?: Suggestion[] | undefined
) => {
  return suggestions?.filter(suggestion => suggestion.value !== value)
}

export const removeDuplicateSuggestions = (suggestions?: Suggestion[]) => {
  const seen = new Set()
  return suggestions?.filter(item =>
    seen.has(item.label) ? false : seen.add(item.label)
  )
}

export const formatBookResponseToSuggestions = (
  data?: Book[]
): Suggestion[] | undefined => {
  const formatted = data?.map(each => ({
    label: each.volumeInfo.title,
    value: each.id
  }))
  return removeDuplicateSuggestions(formatted)
}
