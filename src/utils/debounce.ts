export const debounceTime = <Args, ReturnType>(
  fn: (arg: Args) => Promise<ReturnType> | ReturnType,
  milliseconds: number
): ((args: Args) => Promise<ReturnType>) => {
  let timeout: number | undefined

  return (args: Args): Promise<ReturnType> => {
    return new Promise(resolve => {
      if (timeout) {
        clearTimeout(timeout)
      }

      timeout = Number(
        setTimeout(() => {
          resolve(fn(args))
        }, milliseconds)
      )
    })
  }
}
