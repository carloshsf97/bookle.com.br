import { Suggestion } from '@/components'

interface PriceTags {
  amount?: number
  currencyCode?: string
}

type Saleability = 'FOR_SALE' | 'NOT_FOR_SALE'

export interface Book {
  etag: string
  id: string
  kind: string
  saleInfo: {
    buyLink: string
    country: string
    isEbook: boolean
    saleability: Saleability
    listPrice?: PriceTags
    retailPrice?: PriceTags
  }
  searchInfo: {
    textSnippet: string
  }
  volumeInfo: {
    title: string
    subtitle: string
    authors: string[]
    categories: string[]
    description: string
    previewLink: string
    pageCount: number
    imageLinks: {
      smallThumbnail: string
      thumbnail: string
    }
    infoLink: string
    language: string
  }
  accessInfo?: {
    webReaderLink?: string
    embeddable?: boolean
    epub?: {
      isAvailable?: boolean
    }
    pdf?: {
      isAvailable?: boolean
      acsTokenLink?: string
    }
  }
}
export interface BooksResponse {
  books: Book[]
  totalItems: number
}

export interface SuggestionResponse {
  suggestions: Suggestion[]
  totalItems: number
}
