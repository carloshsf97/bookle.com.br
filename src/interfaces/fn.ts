import { ChangeEvent } from 'react'

export type OnChange<T> = ChangeEvent<T>
