import React, { ChangeEvent, useMemo, useRef } from 'react'
import { SearchInput } from '@/components'
import { Container, ListContainer, List, ListItem } from './styles'
import { AiOutlineClose } from 'react-icons/ai'

export interface Suggestion {
  label?: string
  value?: string
}
interface Props {
  suggestions?: Suggestion[]
  value: string
  isAbsolute?: boolean
  onSearchClear?: () => void
  onClick?: () => void
  onChange?: (e: ChangeEvent<HTMLInputElement>) => void
  onSuggestionClear?: (value?: string) => void
  onSuggestionClick?: (label?: string) => void
}

export const SearchModal: React.VFC<Props> = ({
  onChange,
  onClick,
  onSearchClear,
  onSuggestionClear,
  onSuggestionClick,
  value,
  isAbsolute,
  suggestions = []
}) => {
  const isListOpen = useMemo(() => {
    return suggestions.length > 0
  }, [suggestions.length])

  const ref = useRef(null)

  return (
    <Container>
      <SearchInput
        onChange={onChange}
        onClear={onSearchClear}
        onClick={onClick}
        value={value}
        fullWidth
        config={{ bottomFlat: isListOpen }}
      />
      <ListContainer isAbsolute={isAbsolute} isOpen={isListOpen}>
        {isListOpen && (
          <>
            <div className="line" />

            <List>
              {suggestions?.slice(0, 5).map((item, index) => (
                <ListItem ref={ref} key={index}>
                  <div
                    onClick={() =>
                      onSuggestionClick
                        ? onSuggestionClick(item.label)
                        : undefined
                    }
                  >
                    {item.label}
                  </div>
                  <AiOutlineClose
                    onClick={() =>
                      onSuggestionClear
                        ? onSuggestionClear(item.value)
                        : undefined
                    }
                  />
                </ListItem>
              ))}
            </List>
          </>
        )}
      </ListContainer>
    </Container>
  )
}
