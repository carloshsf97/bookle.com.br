import styled, { css } from 'styled-components'
import { motion } from 'framer-motion'

export const Container = styled(motion.div)`
  width: 100%;
  position: relative;
`

interface ListProps {
  isOpen?: boolean
  isAbsolute?: boolean
}

export const ListContainer = styled(motion.div)<ListProps>`
  ${({ theme, isOpen, isAbsolute }) => css`
    background: #ffffff;
    height: ${isOpen ? '100vh' : '0'};
    max-height: 270px;
    width: 100%;
    margin-top: -6px;
    position: ${isAbsolute ? 'absolute' : 'unset'};
    z-index: 5;

    display: flex;
    align-items: center;
    flex-direction: column;
    border-radius: 0 0 1.5rem 1.5rem;

    .line {
      width: 90%;
      height: 1px;
      margin-top: 0.6rem;
      background: ${theme.colors.text[0]};
      position: relative;
      z-index: 2;
    }
  `}
`

export const List = styled(motion.ul)`
  width: 100%;
  padding: 0.75rem 0;
  list-style-type: none;
`

export const ListItem = styled(motion.li)`
  ${({ theme }) => css`
    cursor: pointer;
    position: relative;
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
    padding-right: 3rem;
    transition: 60ms ease;
    display: flex;
    align-items: center;

    div {
      padding: 0.75rem 0;
      width: 100%;
      height: 100%;
      max-width: 490px;
      padding-left: 1.5rem;
    }

    svg {
      position: absolute;
      right: 1.5rem;
      color: #b4b4b4;
      z-index: 2;

      &:hover {
        color: ${theme.colors.secondary[0]};
      }
    }

    &:hover {
      transition: 120ms ease;
      background-color: #00000010;
    }
  `}
`
