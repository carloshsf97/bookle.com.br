import React, { useState, useEffect } from 'react'
import { SearchModal, Button, Suggestion, Logo } from '@/components'
import { debounceTime } from '@/utils'

import { Container, Content, ButtonsContainer } from './styles'
import { getSuggestionsByKeyword } from '@/services'
import { useRouter } from 'next/router'
import { removeSuggestionFromList } from '@/utils/suggestions'
import { useLocalState } from '@/hooks'
import { Filters } from '@/interfaces'

const Home: React.VFC = () => {
  const router = useRouter()
  const [setFilter] = useLocalState<Filters>('all', 'results-filter')
  const [searchValue, setSearchValue] = useState<string>('')
  const [suggestions, setSuggestions] = useState<Suggestion[]>([])

  useEffect(() => {
    setFilter('all')
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const getDebounceData = debounceTime(
    (value: string) => getSuggestionsByKeyword(value),
    600
  )

  const handleSearchInput = (search: string) => {
    setSearchValue(search)
    handleBooksSearch(search)
  }

  const handleBooksSearch = async (search: string) => {
    if (search.length < 2) return
    try {
      const response = await getDebounceData(search)
      setSuggestions(response.suggestions)
    } catch (err) {
      console.error(err)
    }
  }

  const onSearchClear = () => {
    setSearchValue('')
    setSuggestions([])
  }

  const onRemoveSuggestion = (value?: string) => {
    setSuggestions(current => removeSuggestionFromList(value, current) || [])
  }

  const handleSearch = (search?: string) => {
    if (search?.length) {
      router.push(`/results/${search}`)
    }
  }

  return (
    <Container>
      <Content>
        <Logo onClick={router.reload} />
        <SearchModal
          value={searchValue}
          suggestions={suggestions}
          onChange={e => handleSearchInput(e.target.value)}
          onSearchClear={() => onSearchClear()}
          onSuggestionClear={value => onRemoveSuggestion(value)}
          onSuggestionClick={value => handleSearch(value)}
        />
        <ButtonsContainer>
          <Button onClick={() => handleSearch(searchValue)}>Pesquisar</Button>
          <Button
            onClick={
              suggestions.length === 0
                ? undefined
                : () => router.push(`/book/${suggestions[0].value}`)
            }
          >
            Estou com sorte
          </Button>
        </ButtonsContainer>

        <p>Desenvolvido por Carlos Henrique usando Next.Js</p>
      </Content>
    </Container>
  )
}

export default Home
