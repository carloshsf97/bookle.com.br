import styled, { css } from 'styled-components'
import { motion } from 'framer-motion'

export const Container = styled(motion.div)`
  width: 100%;
  min-height: 100vh;

  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;

  padding: 4rem 1rem;
`

export const Content = styled(motion.div)`
  ${({ theme }) => css`
    width: 100%;
    height: 100vh;
    max-height: 440px;
    max-width: ${theme.grid.content};
    position: relative;

    display: flex;
    flex-direction: column;
    align-items: center;

    h1 {
      letter-spacing: 8px;
      font-size: 4.5rem;
      margin-bottom: 3rem;
    }

    p {
      margin-top: 2rem;
      font-size: 0.75rem;
      color: #e7e7e7;
      text-align: center;
    }
  `}
`

export const ButtonsContainer = styled(motion.div)`
  display: flex;
  width: 100%;
  justify-content: center;
  margin-top: 1.5rem;
  flex-wrap: wrap;

  > button {
    margin: 0.5rem;
  }
`
