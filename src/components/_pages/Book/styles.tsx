import { motion } from 'framer-motion'
import styled, { css } from 'styled-components'

export const Container = styled.div`
  ${({ theme }) => css`
    min-height: 100vh;

    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 6rem 1rem;
    padding-top: 3rem;

    .book-cover {
      min-width: 280px;
      max-width: 360px;
      max-height: 340px;
      object-fit: cover;
    }

    @media screen and (max-width: ${theme.breakpoint.medium}) {
      padding: 4rem 1rem;
    }
  `}
`

interface GridProps {
  flex?: boolean
  wrap?: boolean
}

export const Grid = styled.div<GridProps>`
  ${({ theme, flex, wrap }) => css`
    width: 100%;
    display: ${flex ? 'flex' : 'block'};
    max-width: ${theme.grid.content};
    flex-wrap: ${wrap ? 'wrap' : 'nowrap'};
  `}
`

export const Header = styled.div`
  ${({ theme }) => css`
    h1 {
      color: ${theme.colors.text[0]};
    }

    h3 {
      font-size: 1.5rem;
      color: ${theme.colors.text[2]};
      margin-bottom: 0.5rem;
    }

    h5 {
      color: ${theme.colors.text[2]};
      margin-right: 1rem;
      margin-bottom: 0.5rem;
    }

    @media screen and (max-width: ${theme.breakpoint.medium}) {
      h1 {
        font-size: 2rem;
      }
    }
  `}
`

export const Content = styled.div`
  ${({ theme }) => css`
    margin-top: 2rem;
    display: flex;
    flex-direction: column;

    .report-Link {
      display: flex;
      align-items: center;
      font-size: 0.75rem;
      cursor: pointer;
      transition: 140ms ease;
      width: fit-content;
      svg {
        margin: 0;
        margin-right: 1rem;
      }

      &:hover {
        color: #ffca58;
        transition: 140ms ease;

        svg {
          color: #ffca58;
        }
      }
    }

    .book-list-title {
      margin-left: 2rem;
      margin-bottom: 1rem;
      font-weight: 600;
    }

    .book-list {
      margin-left: 3rem;
      color: ${theme.colors.text[0]};

      li {
        margin-bottom: 0.5rem;
        font-size: 1rem;

        :last-child {
          margin-bottom: 2rem;
        }
      }

      a {
        color: #ffca58;
        cursor: pointer;
        &:hover {
          opacity: 0.6;
        }
      }
    }

    .description {
      margin-top: 1.5rem;
      color: ${theme.colors.text[0]};

      line-height: 2rem;
      font-size: 1.25rem !important;
    }
    svg {
      color: white;
      align-self: center;
      margin-top: 2rem;
      opacity: 0.6;
      cursor: pointer;
      transition: 140ms ease;

      &:hover {
        transition: 140ms ease;
        opacity: 1;
      }
    }

    @media screen and (max-width: ${theme.breakpoint.medium}) {
      .book-list-title,
      .book-list {
        display: none;
      }
    }
  `}
`

export const SeeMore = styled(motion.button)`
  ${({ theme }) => css`
    background: ${theme.colors.text[0]};
    width: fit-content;
    align-self: center;
    margin-top: 2rem;
    padding: 0.75rem 2rem;
    border-radius: 3rem;
    font-size: 1rem;
    font-weight: 600;
    opacity: 0.6;
    transition: 140ms ease;
    border: none;

    &:hover {
      opacity: 1;
      transition: 140ms ease;
      cursor: pointer;
    }
  `}
`

interface TagProps {
  isVisible?: boolean
}

export const Tag = styled(motion.a)<TagProps>`
  ${({ theme, isVisible }) => css`
    align-items: center;
    gap: 1rem;
    background: none;
    border: none;
    color: ${theme.colors.text[0]};
    display: ${isVisible ? 'flex' : 'none'};
    margin-right: 1rem;
    margin-bottom: 1rem;
    border-right: 1px solid ${theme.colors.text[0]};
    padding-right: 1rem;

    :last-child {
      border-right: none;
    }

    &:hover {
      opacity: 0.6;
    }

    svg {
      margin: 0;
      font-size: 1.5rem;
      color: ${theme.colors.text[0]};

      &:hover {
        opacity: 0.6;
      }
    }
  `}
`
