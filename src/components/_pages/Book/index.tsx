import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { Container, Grid, Header, Content, SeeMore, Tag } from './styles'
import { useRouter } from 'next/router'
import { getBookById } from '@/services'
import { Book } from '@/interfaces'
import { truncateString } from '@/utils'
import { MdPictureAsPdf, MdSell } from 'react-icons/md'
import { BsArrowUpCircle, BsFillExclamationTriangleFill } from 'react-icons/bs'
import { IoIosBook } from 'react-icons/io'

const BookPage: React.VFC = () => {
  const router = useRouter()
  const { id } = router.query
  const bookId = id?.toString()

  const [book, setBook] = useState<Book>()
  const [truncateAt, setTruncateAt] = useState<number>(300)
  const descriptionTrucateOptions = useMemo(() => {
    return { length: book?.volumeInfo.description.length || 0, truncateAt }
  }, [book?.volumeInfo.description.length, truncateAt])

  const getBookDetails = useCallback(async () => {
    try {
      const response = await getBookById(bookId)
      setBook(response as Book)
    } catch (err) {
      console.error(err)
    }
  }, [bookId])

  useEffect(() => {
    if (!id) return
    getBookDetails()
  }, [getBookDetails, id])

  return (
    <Container>
      <Grid>
        <Header>
          <h1>{truncateString(book?.volumeInfo.title || '', 40)}</h1>
          <h3>{truncateString(book?.volumeInfo.subtitle || '', 40)}</h3>
          <Grid flex wrap>
            {book?.volumeInfo.authors.map((author, index) => (
              <h5 key={index}>
                {author}
                {index < book?.volumeInfo.authors.length - 1 && ','}
              </h5>
            ))}
          </Grid>
        </Header>
        <Content>
          <Grid flex>
            <img
              className="book-cover"
              src={book?.volumeInfo.imageLinks.thumbnail}
            />
            <div>
              <p className="book-list-title">Detalhes:</p>
              <ul className="book-list">
                {book?.saleInfo.isEbook && <li>eBook disponível.</li>}
                {book?.saleInfo.saleability === 'FOR_SALE' ? (
                  <li>Disponível para venda.</li>
                ) : (
                  <li>Indisponível para venda.</li>
                )}
                {book?.saleInfo.retailPrice && (
                  <li>
                    Preço: &nbsp;
                    <strong>
                      {book?.saleInfo.retailPrice.currencyCode} &nbsp;
                      {book?.saleInfo.retailPrice.amount}
                    </strong>
                  </li>
                )}
                {book?.volumeInfo.pageCount && (
                  <li>{book?.volumeInfo.pageCount} páginas.</li>
                )}
              </ul>
              {book?.volumeInfo.categories && (
                <>
                  <p className="book-list-title">Categorias:</p>
                  <ul className="book-list">
                    {book.volumeInfo.categories
                      .slice(0, 3)
                      .map((categorie, index) => (
                        <li key={index}>{truncateString(categorie, 20)}</li>
                      ))}
                  </ul>
                </>
              )}
            </div>
          </Grid>
          <Grid flex wrap className="mrg-top-02">
            <Tag
              isVisible={book?.accessInfo?.pdf?.isAvailable}
              href={book?.accessInfo?.pdf?.acsTokenLink}
            >
              <MdPictureAsPdf />
              Baixar PDF
            </Tag>
            <Tag
              target="_blank"
              rel="noreferrer noopener"
              isVisible={!!book?.saleInfo.buyLink}
              href={book?.saleInfo.buyLink}
            >
              <MdSell />
              Comprar eBook
            </Tag>
            <Tag
              target="_blank"
              rel="noreferrer noopener"
              isVisible={!!book?.accessInfo?.webReaderLink}
              href={book?.accessInfo?.webReaderLink}
            >
              <IoIosBook />
              Preview
            </Tag>
          </Grid>
          <p className="report-Link">
            <BsFillExclamationTriangleFill />
            Reportar link quebrado
          </p>
          <div
            className="description"
            dangerouslySetInnerHTML={{
              __html: truncateString(
                book?.volumeInfo.description || '',
                descriptionTrucateOptions.truncateAt
              )
            }}
          />
          {descriptionTrucateOptions.length > 300 && (
            <>
              {truncateAt === 0 ? (
                <BsArrowUpCircle onClick={() => setTruncateAt(300)} size={32} />
              ) : (
                <SeeMore onClick={() => setTruncateAt(0)}>Ver mais</SeeMore>
              )}
            </>
          )}
        </Content>
      </Grid>
    </Container>
  )
}

export default BookPage
