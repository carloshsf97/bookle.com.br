import styled, { css, DefaultTheme } from 'styled-components'
import { motion } from 'framer-motion'

export const Container = styled(motion.div)`
  ${() => css`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 101vh;
  `}
`

export const Header = styled(motion.div)`
  ${({ theme }) => css`
    position: sticky;
    width: 100%;
    height: 188px;

    display: flex;
    justify-content: center;
    align-items: flex-end;
    padding: 0 1rem;

    border-bottom: 1px solid #403b6e;
    top: -60px;
    background: ${theme.colors.background[0]};
    z-index: 1;

    h4 {
      font-size: 0.75rem;
      font-weight: 400;
      color: #cacaca9d;
      background: ${theme.colors.background[0]};
      width: 100%;
      max-width: 1200px;
      margin-top: 1px;
      padding: 1rem 2rem;
      padding-bottom: 1.5rem;
      margin-left: -1rem;

      position: absolute;
    }
  `}
`

interface GridProps {
  flex?: boolean
}

export const Grid = styled.div<GridProps>`
  ${({ theme, flex }) => css`
    width: 100%;
    display: ${flex ? 'flex' : 'block'};
    max-width: ${theme.grid.container};
  `}
`

export const FilterContainer = styled.div`
  ${() => css`
    margin-top: 1.5rem;
    white-space: nowrap;
    overflow-x: scroll;
    overflow-y: hidden;

    ::-webkit-scrollbar {
      width: 0;
      height: 0;
      background: transparent;
    }
  `}
`

interface NavLinkProps {
  isActive?: boolean
}

export const NavLink = styled.button<NavLinkProps>`
  ${({ isActive }) => css`
    color: ${isActive ? 'white' : '#c9c9c9'};
    background: none;
    border: none;
    margin: 0 0.5rem;
    cursor: pointer;
    padding: 0 0.5rem;
    padding-bottom: 0.75rem;
    margin-bottom: -1px;
    border-bottom: 2px solid ${isActive ? 'red' : 'rgba(0, 0, 0, 0)'};

    :hover {
      color: white;
      border-bottom: 2px solid ${isActive ? 'red' : '#ff4040'};
    }
  `}
`

interface ContentProps {
  flex?: boolean
}

const flexCss = (theme: DefaultTheme) => css`
  display: flex;
  flex-wrap: wrap;
  gap: 1.5rem;

  @media screen and (max-width: ${theme.breakpoint.medium}) {
    justify-content: center;
  }
`

export const Content = styled.ul<ContentProps>`
  ${({ flex, theme }) => css`
    margin-top: 3.5rem;
    padding: 1rem;
    p {
      padding: 0.5rem 0;
    }
    ${flex && flexCss(theme)};
  `}
`

export const Result = styled.div`
  ${() => css`
    margin-bottom: 1rem;
    max-width: 640px;
    width: calc(100% - 2rem);

    .title {
      display: flex;
      align-items: flex-start;
    }
    svg {
      margin-left: 1.5rem;
      color: #ffffff45;
      transition: 140ms ease;
      cursor: pointer;

      &:hover {
        color: white;
        transition: 140ms ease;
      }
    }
    h3 {
      font-size: 1.5rem;
      margin: 0;
      cursor: pointer;
    }
    p {
      color: #c2c2c2;
    }

    .icon {
      margin-top: 0.5rem;
      width: 1rem;
    }
    .fill {
      color: rgb(255, 58, 58) !important;
    }
  `}
`

export const BookCover = styled.div`
  ${() => css`
    width: 260px;
    height: 340px;
    border-radius: 0.25rem;
    margin-bottom: 4rem;
    text-align: center;
    color: white;
    cursor: pointer;

    h5 {
      margin-top: 0.5rem;
    }

    img {
      width: 100%;
      height: 100%;
      object-fit: cover;
      border-radius: 0.25rem;
    }
  `}
`
export const SearchContainer = styled.div`
  position: relative;
  max-width: 560px;
  z-index: 5;
  display: flex;

  .search {
    margin-left: 1rem;
    transition: 140ms ease;
    cursor: pointer;
    color: white;

    &:hover {
      opacity: 0.6;
      transition: 140ms ease;
    }
  }
`
