import React, { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import {
  Container,
  Grid,
  Header,
  FilterContainer,
  NavLink,
  Content,
  Result,
  BookCover,
  SearchContainer
} from './styles'

import { AiOutlineHeart, AiFillHeart } from 'react-icons/ai'
import { IoSearchCircle } from 'react-icons/io5'
import { Logo, SearchModal, Suggestion } from '@/components'
import { getBooksByKeywords, getSuggestionsByKeyword } from '@/services'
import { Book } from '@/interfaces'
import { useGlobalContext } from '@/contexts/globalContext'
import {
  debounceTime,
  formatBookResponseToSuggestions,
  removeDuplicateSuggestions,
  removeSuggestionFromList,
  truncateString
} from '@/utils'
import Overlay from '@/components/atoms/Overlay'
import { useLocalState } from '@/hooks'
import { Filters } from '@/interfaces/enum'

interface Link {
  type: Filters
  label: string
}

const links = [
  { type: 'all', label: 'Todos' },
  { type: 'images', label: 'Imagens' },
  { type: 'download', label: 'Download' },
  { type: 'ebook', label: 'eBooks' },
  { type: 'favorites', label: 'Favoritos' }
] as Link[]

const Results: React.VFC = () => {
  const router = useRouter()
  const { toggleBookFromFavorites, favorites, books } = useGlobalContext()
  const { search } = router.query

  const [setFilter, filter] = useLocalState<Filters>('all', 'results-filter')
  const [label, setLabel] = useState<string>()
  const [results, setResults] = useState<Book[]>()
  const [filteredResults, setFilteredResults] = useState<Book[]>()
  const [resultsLength, setResultsLength] = useState<number>()
  const [suggestions, setSuggestions] = useState<Suggestion[]>()
  const [searchValue, setSearchValue] = useState<string | undefined>(
    search?.toString()
  )

  const getDebounceData = debounceTime(
    (value: string) => getSuggestionsByKeyword(value),
    600
  )

  const getBooks = async (search?: string) => {
    try {
      const response = await getBooksByKeywords(search)
      setResults(response.books)
      setFilteredResults(response.books)
      setResultsLength(response.totalItems)
      setSuggestions(undefined)
    } catch (err) {
      console.error(err)
    }
  }

  const getBooksSuggestions = async (search: string) => {
    setSearchValue(search)
    try {
      const response = await getDebounceData(search)
      setSuggestions(response.suggestions)
    } catch (err) {
      console.error(err)
    }
  }

  const handleSearch = (search?: string) => {
    if (search?.length) {
      setSearchValue(search)
      setSuggestions(undefined)
      getBooks(search)
      router.push(`/results/${search}`)
    }
  }

  useEffect(() => {
    if (searchValue) {
      getBooks(searchValue)
    } else {
      router.push('/')
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const setFirstSuggestions = () => {
    if (suggestions) return
    setSuggestions(
      removeDuplicateSuggestions(formatBookResponseToSuggestions(results))
    )
  }

  const handleFilter = (filter: Filters) => {
    setFilter(filter)
    switch (filter) {
      case 'all':
        setFilteredResults(results)
        setLabel(`Aproximadamente ${resultsLength} resultados`)
        break
      case 'favorites':
        setFilteredResults(books)
        setLabel('Mostrando seus resultados favoritos')
        break
      case 'ebook':
        setFilteredResults(results?.filter(book => book.saleInfo.isEbook))
        setLabel('Mostrando resultados apenas para "eBooks"')
        break
      default:
        setFilteredResults(results)
        setLabel(`Aproximadamente ${resultsLength} resultados`)
        break
    }
  }

  const handleSuggestionClear = (value?: string) => {
    setSuggestions(current => removeSuggestionFromList(value, current))
  }

  const handleSearchClear = () => {
    setSuggestions(undefined)
    setSearchValue('')
  }

  return (
    <Container>
      <Header>
        <Grid>
          <Logo onClick={() => router.push('/')} />
          <SearchContainer>
            <SearchModal
              isAbsolute
              value={searchValue || ''}
              onChange={e => getBooksSuggestions(e.target.value)}
              onSearchClear={() => handleSearchClear()}
              onSuggestionClear={value => handleSuggestionClear(value)}
              onSuggestionClick={value => handleSearch(value)}
              suggestions={suggestions}
              onClick={setFirstSuggestions}
            />
            <IoSearchCircle
              onClick={() => handleSearch(searchValue)}
              size={48}
              className="search"
            />
          </SearchContainer>
          <FilterContainer>
            {links.map((link, index) => (
              <NavLink
                onClick={() => handleFilter(link.type)}
                isActive={filter === link.type}
                key={index}
              >
                {link.label}
              </NavLink>
            ))}
          </FilterContainer>
          <h4>{label}</h4>
        </Grid>
      </Header>
      <Grid>
        <Content flex={filter === 'images'}>
          {filteredResults?.map((book, index) =>
            filter === 'images' ? (
              <BookCover onClick={() => router.push(`/book/${book.id}`)}>
                <img src={book.volumeInfo.imageLinks.thumbnail} />
                <h5>{book.volumeInfo.title}</h5>
              </BookCover>
            ) : (
              <Result key={index}>
                <div className="title">
                  <h3 onClick={() => router.push(`/book/${book.id}`)}>
                    {truncateString(book.volumeInfo.title, 45)}
                  </h3>
                  <div
                    className="icon"
                    onClick={() =>
                      toggleBookFromFavorites &&
                      toggleBookFromFavorites(book.id, book)
                    }
                  >
                    {favorites?.includes(book.id) ? (
                      <AiFillHeart size={22} className="fill" />
                    ) : (
                      <AiOutlineHeart size={22} />
                    )}
                  </div>
                </div>
                <p>{book.volumeInfo.subtitle}</p>
              </Result>
            )
          )}
        </Content>
      </Grid>
      <Overlay
        isVisible={!!suggestions}
        close={() => setSuggestions(undefined)}
      />
    </Container>
  )
}

export default Results
