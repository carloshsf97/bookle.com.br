import styled, { css } from 'styled-components'
import { OverlayProps } from './interface'

export const Container = styled.div<OverlayProps>`
  ${({ isVisible }) => css`
    /* content: ''; */
    position: fixed;
    top: 0;
    left: 0;
    width: 99vw;
    height: 99vh;
    /* backdrop-filter: blur(5px); */
    display: ${isVisible ? 'block' : 'none'};
  `}
`
