import React from 'react'
import { OverlayProps } from './interface'
import { Container } from './styles'

const Overlay = ({ close, isVisible, id }: OverlayProps) => {
  return (
    <Container
      data-testid="overlay"
      onClick={close}
      isVisible={isVisible}
      id={id}
    />
  )
}

export default Overlay
