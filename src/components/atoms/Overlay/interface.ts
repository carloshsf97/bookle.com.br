export type OverlayProps = {
  id?: string
  isVisible: boolean
  close?: () => void
}
