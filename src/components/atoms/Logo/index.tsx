import React from 'react'
import { Container } from './styles'

interface LogoProps {
  onClick?: () => void
}

export const Logo: React.VFC<LogoProps> = ({ onClick }) => {
  return (
    <Container onClick={onClick}>
      <h1>
        B<span className="orange">o</span>
        <span className="blue">o</span>k<span className="green">l</span>
        <span className="orange">e</span>
      </h1>
    </Container>
  )
}
