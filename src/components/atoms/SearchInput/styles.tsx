import styled, { css } from 'styled-components'
import { motion } from 'framer-motion'
import { SearchInputConfig } from '@/components'

interface ContainerProps {
  fullWidth?: boolean
  config?: SearchInputConfig
}

export const Container = styled(motion.div)<ContainerProps>`
  ${({ fullWidth, config }) => css`
    position: relative;
    display: flex;
    align-items: center;
    width: 100%;
    height: 48px;
    border-radius: ${config?.bottomFlat ? '1.5rem 1.5rem 0 0' : '1.5rem'};
    max-width: ${fullWidth ? '100%' : '480px'};
    background: #ffffff;
  `}
`
export const Element = styled(motion.input)`
  background: none;
  border: none;
  width: 100%;
  height: 100%;
  padding: 0 2rem;
  padding-right: 3.25rem;

  font-size: 1rem;

  :focus {
    outline: none;
  }
`
export const Label = styled(motion.label)``
export const Icon = styled(motion.div)`
  ${({ theme }) => css`
    position: absolute;
    right: 1.5rem;
    color: #aaaaaa;
    margin-top: 0.5rem;
    cursor: pointer;
    transition: 140ms ease;

    &:hover {
      transition: 140ms ease;
      color: ${theme.colors.secondary[0]};
    }
  `}
`
