import React, { ChangeEvent } from 'react'
import { Container, Element, Label, Icon } from './styles'

import { AiOutlineClose, AiOutlineSearch } from 'react-icons/ai'

const default_config = {
  bottomFlat: false
}

export type SearchInputConfig = typeof default_config

interface SearchInputProps {
  label?: string
  fullWidth?: boolean
  onChange?: (event: ChangeEvent<HTMLInputElement>) => void
  onClear?: () => void
  onClick?: () => void
  config?: SearchInputConfig
  value?: string
}

export const SearchInput: React.VFC<SearchInputProps> = ({
  label,
  fullWidth,
  config,
  value = '',
  onChange,
  onClear,
  onClick
}) => {
  return (
    <Container onClick={onClick} fullWidth={fullWidth} config={config}>
      <Label>{label}</Label>
      <Element value={value} onChange={onChange} />
      <Icon onClick={value?.length === 0 ? undefined : onClear}>
        {value?.length === 0 ? (
          <AiOutlineSearch size={22} />
        ) : (
          <AiOutlineClose size={22} />
        )}
      </Icon>
    </Container>
  )
}
