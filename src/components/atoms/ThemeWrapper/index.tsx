import React from 'react'
import { ThemeProvider } from 'styled-components'
import GlobalStyle from '../../../styles/global'
import dark from '../../../styles/dark'
import { ThemeProps } from './interface'

const ThemeWrapper = ({ children, ...props }: ThemeProps) => {
  return (
    <ThemeProvider theme={dark} {...props}>
      {children}
      <GlobalStyle />
    </ThemeProvider>
  )
}

export default ThemeWrapper
