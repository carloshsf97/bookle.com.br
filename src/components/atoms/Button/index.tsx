import React from 'react'
import { Container } from './styles'

interface ButtonProps {
  children?: React.ReactNode
  onClick?: () => void
}

export const Button: React.VFC<ButtonProps> = ({ children, onClick }) => {
  return <Container onClick={onClick}>{children}</Container>
}
