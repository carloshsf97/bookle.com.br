import styled, { css } from 'styled-components'
import { motion } from 'framer-motion'

export const Container = styled(motion.button)`
  ${({ theme }) => css`
    height: 2.25rem;
    width: 12rem;
    border-radius: 0.5rem;
    background: ${theme.colors.tertiary[0]};
    cursor: pointer;
    font-size: 1rem;
    border: none;
    transition: 140ms ease;

    &:hover {
      opacity: 0.7;
      transition: 140ms ease;
    }
  `}
`
