export default {
  grid: {
    container: '80rem',
    content: '34rem',
    gutter: '2rem'
  },
  border: {
    radius: '0.5rem'
  },
  font: {
    family: "'Open Sans', sans-serif",
    weight: {
      light: 300,
      normal: 400,
      bold: 600
    }
  },
  colors: {
    primary: ['#864879'],
    secondary: ['#E9A6A6'],
    tertiary: ['#EEEEEE'],
    text: ['#EEEEEE', '#dbdbdb', '#b8b8b8'],
    background: ['#1F1D36', '#1F1D36']
  },
  layers: {
    base: 1,
    menu: 3,
    overlay: 5,
    modal: 10,
    alwaysOnTop: 15
  },
  breakpoint: {
    small: '450px',
    medium: '768px',
    large: '1170px',
    huge: '1440px'
  },
  shadow: {
    centered: '0px 5px 20px 5px rgba(0,0,0,0.15)'
  },
  heights: {
    menu: '80px',
    footer: '340px'
  },
  screens: {
    sm: '520px'
  }
}
