import { createGlobalStyle, css } from 'styled-components'

const GlobalStyles = createGlobalStyle`
  ${({ theme }) => css`
    * {
      margin: 0;
      padding: 0;
      box-sizing: border-box;
    }

    main {
      height: 100%;
    }

    html,
    body {
      padding: 0;
      min-height: 100%;
      margin: 0;
      font-size: 16px;
      font-weight: 300;
      background-color: ${theme.colors.background[0]};
      font-family: 'Open Sans', sans-serif;
      scroll-behavior: smooth;
    }

    a {
      color: inherit;
      text-decoration: none;
    }

    h1,
    h2,
    h3,
    h4,
    h4,
    h6,
    p {
      color: ${theme.colors.text[0]};
      font-family: 'Poppins', sans-serif;
    }

    h1 {
      font-size: 2.5rem;
      line-height: 3.25rem;
      margin-bottom: 1rem;
    }

    h2 {
      font-size: 2rem;
      font-weight: 700;
    }

    h3 {
      font-size: 1rem;
    }

    p,
    span {
      line-height: 1.75rem;
    }

    input,
    textarea,
    button,
    select,
    img,
    a {
      -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
    }

    .capitalize {
      text-transform: capitalize;
    }

    .lowercase {
      text-transform: lowercase;
    }

    .mrg-top-01 {
      margin-top: 1rem;
    }
    .mrg-top-02 {
      margin-top: 2rem;
    }
    .mrg-top-03 {
      margin-top: 3rem;
    }
    .mrg-top-04 {
      margin-top: 4rem;
    }
    .mrg-top-05 {
      margin-top: 5rem;
    }
    .mrg-top-06 {
      margin-top: 6rem;
    }
    .mrg-bottom-01 {
      margin-bottom: 1rem;
    }
    .mrg-bottom-02 {
      margin-bottom: 2rem;
    }
    .mrg-bottom-03 {
      margin-bottom: 3rem;
    }
    .mrg-v-01 {
      margin-top: 2rem;
      margin-bottom: 2rem;
    }
    .mrg-v-02 {
      margin-top: 2rem;
      margin-bottom: 2rem;
    }
    .padding-bottom-02 {
      padding-bottom: 2rem;
    }
    .padding-bottom-04 {
      padding-bottom: 4rem;
    }
    .padding-bottom-06 {
      padding-bottom: 6rem;
    }
    .padding-bottom-08 {
      padding-bottom: 8rem;
    }
    .center {
      text-align: center;
    }

    .orange {
      color: orange;
    }

    .blue {
      color: #408cff;
    }

    .green {
      color: green;
    }
  `}
  `

export default GlobalStyles
