export default {
  grid: {
    container: ['80rem', '60rem', '40rem', '25rem'],
    gutter: '2rem'
  },
  border: {
    radius: '0.5rem'
  },
  font: {
    family: "'Open Sans', sans-serif",
    weight: {
      light: 300,
      normal: 400,
      bold: 600
    }
  },
  colors: {
    primary: ['#e8ba00', '#e3c54b', '#F1F7FB'],
    secondary: ['#0f1d59', '#09C199BF'],
    paragraph: ['#fff', '#404040'],
    red: '#B81213',
    lightGray: '#cbcbcb',
    gray: '#8F8F8F',
    darkGray: '#2E2F42',
    background: ['#fafafa', '#223aa1', '#fff', '#F1F7FB'],
    footer: '#0866B7',
    header: '#fff',
    dropcard: '#fafafa',
    postCard: '#fafafa'
  },
  layers: {
    base: 10,
    menu: 20,
    overlay: 30,
    modal: 40,
    alwaysOnTop: 50
  },
  breakpoint: {
    small: '450px',
    medium: '768px',
    large: '1170px',
    huge: '1440px'
  },
  shadow: {
    centered: '0px 5px 20px 5px rgba(0,0,0,0.1)'
  },
  heights: {
    menu: '80px',
    footer: '340px'
  },
  screens: {
    sm: '520px'
  }
}
