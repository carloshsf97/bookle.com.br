import { Book } from '@/interfaces'
import React, {
  createContext,
  Dispatch,
  SetStateAction,
  useContext,
  useEffect,
  useState
} from 'react'

type Props = {
  children: React.ReactNode
}

type ContextType = {
  isDark?: boolean
  setIsDark?: Dispatch<SetStateAction<boolean>>
  favorites?: string[]
  books?: Book[]
  addBookToFavorites?: (e: string, book: Book) => void
  removeBookFromFavorites?: (e: string, book: Book) => void
  toggleBookFromFavorites?: (e: string, book: Book) => void
}

const GlobalContext = createContext<ContextType>({
  isDark: false,
  setIsDark: undefined,
  addBookToFavorites: undefined,
  removeBookFromFavorites: undefined,
  toggleBookFromFavorites: undefined,
  favorites: [],
  books: []
})

const GlobalProvider = ({ children }: Props) => {
  const [isDark, setIsDark] = useState<boolean>(false)
  const [favorites, setFavorites] = useState<string[]>([])
  const [books, setBooks] = useState<Book[]>([])

  useEffect(() => {
    console.log(favorites)
  }, [favorites])

  const addBookToFavorites = (id: string, book: Book) => {
    setFavorites(current => current?.concat(id))
    setBooks(current => current?.concat(book))
  }

  const removeBookFromFavorites = (id: string) => {
    setFavorites(current => current?.filter(book => book !== id))
    setBooks(current => current?.filter(book => book.id !== id))
  }

  const toggleBookFromFavorites = (id: string, book: Book) => {
    if (favorites.includes(id)) {
      removeBookFromFavorites(id)
    } else {
      addBookToFavorites(id, book)
    }
  }

  return (
    <GlobalContext.Provider
      value={{
        isDark,
        setIsDark,
        addBookToFavorites,
        removeBookFromFavorites,
        toggleBookFromFavorites,
        favorites,
        books
      }}
    >
      {children}
    </GlobalContext.Provider>
  )
}

function useGlobalContext() {
  return useContext(GlobalContext)
}

export { GlobalContext, GlobalProvider, useGlobalContext }
