export const fadeIn = {
  initial: {
    opacity: 0
  },
  animate: {
    opacity: 1
  }
}

export const fadeInWaterfall = {
  initial: {
    opacity: 0
  },
  animate: {
    opacity: 1,
    transition: {
      duration: 0.5,
      staggerChildren: 0.3
    }
  }
}

export const longWaterfall = {
  initial: {
    opacity: 0
  },
  animate: {
    opacity: 1,
    transition: {
      delay: 0.45,
      duration: 0.8,
      staggerChildren: 2
    }
  }
}

export const popUp = {
  initial: {
    opacity: 0,
    y: '50px'
  },
  animate: {
    opacity: 1,
    y: 0,
    transition: {
      duration: 0.15
    }
  }
}

export const lazyPopUp = {
  initial: {
    opacity: 0,
    y: '50px'
  },
  animate: {
    opacity: 1,
    y: 0,
    transition: {
      duration: 0.33,
      delay: 0.9
    }
  }
}
export const popUpWaterfall = {
  initial: {
    opacity: 0,
    y: '60px'
  },
  animate: {
    opacity: 1,
    y: 0,
    transition: {
      duration: 0.5,
      staggerChildren: 0.35
    }
  }
}
