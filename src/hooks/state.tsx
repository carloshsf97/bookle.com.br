import { useState, useEffect, SetStateAction, Dispatch } from 'react'
import store from 'store'

export const useLocalState = <T extends unknown>(
  initialValue: unknown,
  name: string
): [Dispatch<SetStateAction<T>>, T] => {
  if (!name) {
    throw new Error('Name must be provided to persist to localStorage')
  }
  const actualInitialValue =
    store.get(name) !== undefined ? store.get(name) : initialValue

  const [value, setValue] = useState<T>(actualInitialValue)

  useEffect(() => {
    store.set(name, value)
  }, [name, value])

  return [setValue, value]
}
